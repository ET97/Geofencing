import point_in_polygon as pinp


def test_point_is_in_polygon_cn():
    x1 = 0.2323
    x2 = 5.12344123
    P = (1.1243, 2.34545)
    V = [(x1, x1), (x1, x2), (x2, x2), (x2, x1)]

    assert pinp.cn_PnPoly(P, V)


def test_point_is_in_polygon_wn():
    x1 = 0.2323
    x2 = 5.12344123
    P = (1.1243, 2.34545)
    V = [(x1, x1), (x1, x2), (x2, x2), (x2, x1)]

    assert pinp.wn_PnPoly(P, V)


def test_point_is_outside_polygon_cn():
    x1 = 0.2323
    x2 = 5.12344123
    P = (11.1243, 2.34545)
    V = [(x1, x1), (x1, x2), (x2, x2), (x2, x1)]

    assert not pinp.cn_PnPoly(P, V)


def test_point_is_outside_polygon_wn():
    x1 = 0.2323
    x2 = 5.12344123
    P = (1.1243, 12.34545)
    V = [(x1, x1), (x1, x2), (x2, x2), (x2, x1)]

    assert not pinp.wn_PnPoly(P, V)