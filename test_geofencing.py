import geofencing as gf

v1 = [(0.0, 0.0), (2.0, 0.0), (2.0, 3.0), (0.0, 3.0)]
v2 = [(10.0, 10.0), (12.0, 10.0), (12.0, 13.0), (10.0, 13.0)]
str1 = "test polygon 1"
str2 = "test polygon 2"
poly1 = dict(vertices=v1, tag=str1)
poly2 = dict(vertices=v2, tag=str2)


def test_creation_get():
    gf1 = gf.Geofencing([poly1])
    assert gf1.get_polygons()[0] == poly1


def test_add_polygon():
    gf1 = gf.Geofencing([poly1])
    gf1.add_polygon(poly2)

    assert len(gf1.get_polygons()) == 2
    assert gf1.get_polygons()[1] == poly2


def test_point_is_inside1():
    gf1 = gf.Geofencing([poly1, poly2])
    p = (0.5, 0.5)
    assert gf1.is_point_inside(p) == str1


def test_point_is_inside2():
    gf1 = gf.Geofencing([poly1, poly2])
    p = (10.5, 10.5)
    assert gf1.is_point_inside(p) == str2


def test_point_is_not_inside():
    gf1 = gf.Geofencing([poly1, poly2])
    p = (8.5, 8.5)
    assert not gf1.is_point_inside(p)