import point_in_polygon as pinp


class Geofencing(object):

    # input parameter 'polygons' is a list of dictionaries with field 'vertices' and 'tag'
    def __init__(self, polygons):
        self.__polygons = polygons

    def get_polygons(self):
        return self.__polygons

    def add_polygon(self, polygon):
        self.__polygons.append(polygon)

# tests, if point is within one of the provided polygons
# input: point, expressed as tuple (x,y)
# output: tag of the first polygon, the provided point is within
    def is_point_inside(self, point):
        for polygon in self.__polygons:
            is_inside = pinp.wn_PnPoly(point, polygon.get("vertices"))
            if is_inside:
                return polygon.get("tag")
        return None